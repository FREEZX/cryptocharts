var App = {
  controller: function() {

  },
  view: function(ctrl) {
    return m('div', [
      m.component(Header),
      m('div', {class: 'container', style: {padding: "0px"}}, [
        m('div', [
          m.component(Chart),
          m.component(Sidebar)
        ])
      ])
    ]);
  }
};

m.mount(document.getElementById('app'), App);