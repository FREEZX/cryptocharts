var Sidebar = {
	controller: function () {
		var ctrl = this;
		
		this.changeSelectedItem = function(e) {
			selectedId = this.no;
			m.redraw();
			e.preventDefault();
			return false;
		}
	},
	view: function (ctrl) {
		var filtered = _.filter(chartData.marketlist(), function(item){
			return _.endsWith(item.pair, '_cny') && item.name != null && (!_.startsWith(item.name, '<')) && item.name !== 'Chinese Yuan';
		});

		var leftSplit = [];
		var rightSplit = [];
		var i;

		_.forEach(filtered, function(link, key){
			var item = m('li', {class: (selectedId === link.no) ? 'active' : ''}, m('a', {href: '#', onclick: ctrl.changeSelectedItem.bind(link)}, link.name));
			if(key<filtered.length/2) {
				leftSplit.push( item );
			}
			else {
				rightSplit.push( item );
			}
		});
		
		return m('div', {class: 'col-md-4', style: "margin-top: 20px"}, [
			m('ul', {class: 'nav nav-pills nav-stacked col-md-6 col-sm-6'}, leftSplit),
			m('ul', {class: 'nav nav-pills nav-stacked col-md-6 col-sm-6'}, rightSplit)
		]);
	}
};
