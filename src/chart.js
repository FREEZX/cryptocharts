var chartData = {
	marketlist: m.prop([])
}

var selectedId = 1;
var corsBase = 'http://'+location.hostname+':3000/?url=';

m.deferred.onerror = function(e) {console.error(e)}

var Chart = {
	controller: function() {
		var ctrl = this;
		this.chartValues = m.prop([]);

		this.chart = null;
		this.loadedId = -1;

		this.actualPair = m.prop('');
		this.actualData = m.prop({});

		this.refreshInterval = null;

		this.getData = function() {
			chartData.marketlist = m.request({
				method: 'GET', 
				url: corsBase+'http://data.bter.com/api/1/marketlist', 
				background: true, 
				initialValue: [],
				unwrapSuccess: function(response) {
					//Fix quirky vals for volume of second currency
					for(var i=0; i<response.data.length; ++i) {
						response.data[i].vol_b = parseFloat(response.data[i].vol_b.replace(',', ''));
					}
					return response.data;
				}
			});
			m.sync([chartData.marketlist]).then(this.refreshChartData).then(m.redraw);
		},

		this.refreshChartData = function() {
			var chartVals = chartData.marketlist()[selectedId-1].plot.map(function(plot) {
				return {
					date: plot[0]*1000,
					value: plot[1]
				}
			});
			ctrl.chartValues(chartVals);
		},

		this.createChart = function(el, isInit, context) {
			var data = [];
			if(chartData.marketlist().length === 0) {
				return;
			}

			if(ctrl.loadedId !== selectedId){
				ctrl.setTicker();
				ctrl.refreshChartData();
				ctrl.loadedId = selectedId;

				ctrl.chart = AmCharts.makeChart( el, {
					height: 400,
					responsive: true,
					type: "serial",
					theme: "light",
					path: "http://www.amcharts.com/lib/3/",
					valueAxes: [ {
						position: "left"
					} ],
					graphs: [ {
						valueField: "value"
					} ],
					chartScrollbar: {
						graph: "g1",
						graphType: "line"
					},
					chartCursor: {
						valueLineEnabled: true,
						valueLineBalloonEnabled: true,
						cursorColor: '#337ab7'
					},
					categoryField: "date",
					categoryAxis: {
						parseDates: true,
						minPeriod: 'mm'
					},
					dataProvider: ctrl.chartValues(),
					export: {
						enabled: true,
						position: "bottom-right"
					}
				});
			}

			ctrl.chart.addListener("zoomed", function (event) {
			  this.zoom = _.omit(event, 'type');
			});
		}

		this.setTicker = function() {
			ctrl.getTickerData();
			clearInterval(ctrl.refreshInterval);
			ctrl.refreshInterval = setInterval(ctrl.getTickerData, 10000);
		}

		this.getTickerData = function() {
			m.request({url: corsBase+'http://data.bter.com/api/1/ticker/'+chartData.marketlist()[selectedId-1].pair, background: true}).then(function(data) {
				ctrl.actualPair(chartData.marketlist()[selectedId-1].pair);
				var cura = chartData.marketlist()[selectedId-1].curr_a.toLowerCase();
				var curb = chartData.marketlist()[selectedId-1].curr_b.toLowerCase();

				chartData.marketlist()[selectedId-1].vol_a = data['vol_'+cura];
				chartData.marketlist()[selectedId-1].vol_b = data['vol_'+curb];

				chartData.marketlist()[selectedId-1].rate = data.last;
				chartData.marketlist()[selectedId-1].rate_percent = data.rate_change_percentage;
				ctrl.actualData(data);

				var date = new Date();
				ctrl.chartValues().push({date: date, value: data.last});
				var startDate = ctrl.chart.startDate;
				var endDate = ctrl.chart.endDate;
				var endIdx = ctrl.chart.endIndex;

				if(endIdx >= ctrl.chart.dataProvider.length-2) {
					endDate = date;
				}
				m.redraw();
				ctrl.chart.validateData();
				ctrl.chart.zoomToDates(startDate, endDate);
			});
		}

		this.getData();
	},
	view: function(ctrl) {
		var selectedChart = chartData.marketlist()[selectedId-1];
		return m('div', {class: 'col-md-8', style: "margin-top: 20px"}, [
			function() {
				if(selectedChart){
					return m('div', [
						m('h1', selectedChart.name)
					]);
				}
			}(),
			m('div', {class: 'jumbotron', config: ctrl.createChart}),
			function() {
				if(selectedChart){
					var negative = _.startsWith(selectedChart.rate_percent, '-');
					var symbol = negative ? '▼' : '▲';

					var dataDivs = [
						m('h2', selectedChart.curr_a + ' / ' + selectedChart.curr_b),
						m('div', {class: 'row'}, [
							m('h4', {class: 'col-sm-4'}, 'Last: '+selectedChart.rate),
							m('h4', {class: 'col-sm-4'}, ['Change: ', m('span', {style: {color: negative ? '#FF0000': '#00FF00'}}, symbol + selectedChart.rate_percent)])
						]),
						m('div', {class: 'row'}, [
							m('h4', {class: 'col-sm-4'}, 'Volume '+selectedChart.curr_a+': '+selectedChart.vol_a),
							m('h4', {class: 'col-sm-4'}, 'Volume '+selectedChart.curr_b+': '+selectedChart.vol_b)
						])
					];

					if(ctrl.actualPair() == selectedChart.pair) {
						dataDivs = dataDivs.concat([
							m('div', {class: 'row'}, [
								m('h4', {class: 'col-sm-4'}, 'Avg: '+ctrl.actualData().avg),
								m('h4', {class: 'col-sm-4'}, 'Low: '+ctrl.actualData().low),
								m('h4', {class: 'col-sm-4'}, 'High: '+ctrl.actualData().high)
							]),
							m('div', {class: 'row'}, [
								m('h4', {class: 'col-sm-4'}, 'Buy: '+ctrl.actualData().buy),
								m('h4', {class: 'col-sm-4'}, 'Sell: '+ctrl.actualData().sell)
							])
						]);
					};
					return m('div', dataDivs);
				}
			}(),
		]);
	}
};