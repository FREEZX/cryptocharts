To run this, follow these instructions:

1. install node.js (which includes the npm command)
2. execute `npm install -g bower` - this installs the bower front-end package manager
2. execute `npm install -g http-server` - this installs a tiny http server that hosts the app (needed for CORS workaround)
3. execute `npm install` - this installs the project's node dependencies
4. execute `bower install` - this installs the project's front-end dependencies (like bootstrap, mithril, etc.)
5. execute `npm start` - this starts the small http server and the CORS proxy. Run this any time when you want to start working on the project
6. open `127.0.0.1:8080` in the browser and witness.